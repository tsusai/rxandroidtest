package com.example.rxandroidtest;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import rx.Observable;

public class MainActivity2 extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final TextView textView = findViewById(R.id.tv);
        Observable.just(textView.getText().toString()).map(s -> s+" Rx!").subscribe(text -> textView.setText(text));
    }
}
