package com.example.rxandroidtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    Subscription sb1;
    Subscription sb2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Observable<Integer> database = Observable.just(1,2,3,4,5)
                .filter( integer -> integer % 2 != 0 );
        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onCompleted() {

                Log.i("RX", "observer1.onCompleted() ");
                if (sb1 != null)
                    Log.i("RX", "sb1.isUnsubscribed() : " + sb1.isUnsubscribed());
                else
                    Log.i("RX", "sb1 is null yet");
            }

            @Override
            public void onError(Throwable e) {

                Log.i("RX", "observer1.onError() " + e);
                if (sb1 != null)
                    Log.i("RX", "sb1.isUnsubscribed() : " + sb1.isUnsubscribed());
                else
                    Log.i("RX", "sb1 is null yet");
            }

            @Override
            public void onNext(Integer s) {

                Log.i("RX", "observer1.onNext() " + s);
                if (sb1 != null)
                    Log.i("RX", "sb1.isUnsubscribed() : " + sb1.isUnsubscribed());
                else
                    Log.i("RX", "sb1 is null yet");

            }
        };
        Observer<Integer> observer2 = new Observer<Integer>() {
            @Override
            public void onCompleted() {

                Log.i("RX", "observer2.onCompleted() ");
            }

            @Override
            public void onError(Throwable e) {

                Log.i("RX", "observer2.onError() " + e);
            }

            @Override
            public void onNext(Integer s) {

                Log.i("RX", "observer2.onNext() " + s);

            }
        };
        Action1<Integer> onNextAction = integer -> Log.i("RX", "onNextAction.call() " + integer);

        database.subscribeOn(Schedulers.newThread());
        try {
            Log.i("RX", "called subscribeOn()");
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        database.observeOn(AndroidSchedulers.mainThread());
        try {
            Log.i("RX", "called observeOn()");
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sb1 = database.subscribe(observer);
        try {
            Log.i("RX", "called subscribe()");
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sb2 = database.subscribe(onNextAction);

        Log.i("RX", "sb1.isUnsubscribed() : " + sb1.isUnsubscribed());

        Log.i("RX", "sb2.isUnsubscribed() : " + sb2.isUnsubscribed());

        sb2 = database.subscribe(onNextAction);

        Log.i("RX", "sb2.isUnsubscribed() : " + sb2.isUnsubscribed());

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (!sb1.isUnsubscribed()) {
            sb1.unsubscribe();
            Log.i("RX", "sb1.unsubscribe()");
        }
        if (!sb2.isUnsubscribed()) {
            sb2.unsubscribe();
            Log.i("RX", "sb2.unsubscribe()");
        }
    }
}